import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Resolve } from '@angular/router/src/interfaces';

@Injectable()
export class BooksService {
  baseUrl = 'http://localhost:49490/api/Books/';
  constructor(
    private _http: Http) { }

  getListAuthor() {
    return this._http.get(this.baseUrl + 'GetListAuthor')
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  getListPublisher() {
    return this._http.get(this.baseUrl + 'GetListPublisher')
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  getListCategory() {
    return this._http.get(this.baseUrl + 'GetListCategory')
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  getBooks() {
    return this._http.get(this.baseUrl + 'GetBooks')
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  getBooksById(id) {
    return this._http.get(this.baseUrl + 'GetBooksById/' + id)
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  saveBooks(books) {
    return this._http.post(this.baseUrl + 'SaveBooks', books)
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  deleteBooks(id) {
    return this._http.delete(this.baseUrl + 'DeleteBooks/' + id)
      .map((response: Response) => response.json())
      .catch(this._errorHandler);
  }

  _errorHandler(error: Response) {
    // tslint:disable-next-line:no-debugger
    debugger;
    console.log(error);
    return Observable.throw(error || 'Internal server error');
  }

}
