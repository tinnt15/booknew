import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../../services/books.service';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html'
})
export class BookListComponent implements OnInit {
  book: Array<any> = [];
  errorMessage: any;
  currentId = 0;

  serarchText = '';

  constructor(private _bookService: BooksService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    // tslint:disable-next-line:curly
    if (this._activatedRoute.snapshot.params['id'])
      // tslint:disable-next-line:radix
      this.currentId = parseInt(this._activatedRoute.snapshot.params['id']);
    this.getBooks();
  }

  getBooks() {
    this._bookService.getBooks().subscribe(
      data => this.book = data,
      error => {
        // tslint:disable-next-line:no-debugger
        debugger;
        this.errorMessage = error;
      }
    );
  }

  add() {
    this._router.navigate(['books/add']);
  }
  edit(id) {
    this._router.navigate(['books/edit/' + id]);
  }
  delete(id) {
    const ans = confirm('Do you want to delete customer with Id: ' + id);
    if (ans) {
      this._bookService.deleteBooks(id)
        .subscribe(data => {
          const index = this.book.findIndex(x => x.id === id);
          this.book.splice(index, 1);
        }, error => this.errorMessage = error);
    }
  }
}
