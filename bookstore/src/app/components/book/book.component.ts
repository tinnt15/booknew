import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html'
})
export class BookComponent implements OnInit {
  booksForm: FormGroup;
  author: Array<any> = [];
  publisher: Array<any> = [];
  cate: Array<any> = [];
  title = 'Add';
  id = 0;
  errorMessage: any;
  submitted = false;
  _ref: any;
  constructor(private _fb: FormBuilder,
    private _avRoute: ActivatedRoute,
    private _bookService: BooksService,
    private _router: Router) {

    if (this._avRoute.snapshot.params['id']) {
      // tslint:disable-next-line:radix
      this.id = parseInt(this._avRoute.snapshot.params['id']);
      console.log(this.id);
      this.title = 'Edit';
    }

    this.booksForm = this._fb.group({
      BookId: 0,
      Title: ['', [Validators.required]],
      CateID: ['', [Validators.required]],
      AuthorID: ['', [Validators.required]],
      PubID: ['', [Validators.required]],
      Summary: ['', [Validators.required]],
      ImgUrl: '',
      Price: ['', [Validators.required]],
      Quantity: ['', [Validators.required]],
      CreatedDate: ['', [Validators.required]],
      ModifiedDate: ['', [Validators.required]]

    });
  }

  ngOnInit() {
    if (this.id > 0) {
      this._bookService.getBooksById(this.id)
        .subscribe(resp => this.booksForm.setValue(resp)
        , error => this.errorMessage = error);
      this._bookService.getListAuthor().subscribe(
        data => this.author = data
      );
      this._bookService.getListPublisher().subscribe(
        data => this.publisher = data
      );
      this._bookService.getListCategory().subscribe(
        data => this.cate = data
      );

    }
  }

  save() {
    // tslint:disable-next-line:no-debugger
    debugger;
    if (!this.booksForm.valid) {
      this.submitted = true;
      return;
    }

    this._bookService.saveBooks(this.booksForm.value)
      .subscribe(BookId => {
        // alert('Saved Successfully!')
        this._router.navigate(['books', { id: BookId }]);
      }, error => this.errorMessage = error);

  }

  cancel() {
    this._router.navigate(['books', { id: this.id }]);
  }

  get name() { return this.booksForm.get('name'); }
  get address() { return this.booksForm.get('address'); }
  get phone() { return this.booksForm.get('phone'); }

}
