﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookAPI.Models
{
    public class BooksJSON
    {
        public int BookID { get; set; }
        public string Title { get; set; }
        public int?  CateID { get; set; }
        public int? AuthorID { get; set; }
        public int? PubID { get; set; }
        public string Summary { get; set; }
        public string ImgUrl { get; set; }
        public double? Price { get; set; }
        public double? Quantity { get; set; }
        public System.DateTime? CreatedDate { get; set; }
        public System.DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public string ISBN { get; set; }
        public string ISBN13 { get; set; }
    }
}