﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BookAPI.DBModel;
using System.Security.Cryptography;
using System.Text;

namespace BookAPI.Controllers
{
    [RoutePrefix("api/account")]
    public class LoginController : ApiController
    {
        BookStoreEntities db = new BookStoreEntities();

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public IHttpActionResult checkuser(UserData userData)
        {
            string hashinput = MD5Hash(userData.password);
            var rs = db.Users.FirstOrDefault(x => x.Email == userData.email && x.Password == hashinput);
            if (rs != null)
            {
                return Ok(true);

            }
            return Ok(false);
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            //get hash result after compute it
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("X"));
            }
            return strBuilder.ToString();
        }
    }
    public class UserData
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
