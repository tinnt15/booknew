﻿using System.Linq;
using System.Web.Http;
using BookAPI.DBModel;

namespace BookAPI.Controllers
{
    public class BooksController : ApiController
    {
         BookStoreEntities  bookStoreContext = new BookStoreEntities();

        [HttpGet]
        [Route("api/Books/GetListAuthor")]
        public IQueryable<Author> GetListAuthor()
        {
            return bookStoreContext.Authors;
        }
        [HttpGet]
        [Route("api/Books/GetListPublisher")]
        public IQueryable<Publisher> GetListPublisher()
        {
            return bookStoreContext.Publishers;
        }

        [HttpGet]
        [Route("api/Books/GetListCategory")]
        public IQueryable<Category> GetListCategory()
        {
            return bookStoreContext.Categories;
        }

        [HttpGet]
        [Route("api/Books/GetBooks")]
        public IQueryable<Book> GetBooks()
        {
            return bookStoreContext.Books;
        }

        [HttpGet]
        [Route("api/Books/GetBooksById/{id}")]
        public IHttpActionResult GetCustomerById(int id)
        {
            var books = bookStoreContext.Books.FirstOrDefault(x => x.BookId == id);
            if (books != null)
                return Ok(books);
            else
                return NotFound();
        }

        [HttpPost]
        [Route("api/Books/SaveBooks")]
        public IHttpActionResult SaveBooks(Book book)
        {
            if (book.BookId > 0)
            {
                var dbbooks = bookStoreContext.Books.FirstOrDefault(x => x.BookId == book.BookId);
                dbbooks.AuthorID = book.AuthorID;
                dbbooks.CateID = book.CateID;
                dbbooks.Title = book.Title;
                dbbooks.Summary = book.Summary;
                dbbooks.Quantity = book.Quantity;
                dbbooks.Price = book.Price;
                dbbooks.ImgUrl = book.ImgUrl;
                dbbooks.IsActive = book.IsActive;
                dbbooks.ISBN13 = book.ISBN13;
                dbbooks.ModifiedDate = book.ModifiedDate;
                dbbooks.PubID = book.PubID;
                dbbooks.CreatedDate = book.CreatedDate;

                bookStoreContext.SaveChanges();
            }
            else
            {
                bookStoreContext.Books.Add(book);
                bookStoreContext.SaveChanges();
            }
            return Ok(book.BookId);
        }
        [HttpDelete]
        [Route("api/Books/DeleteBooks/{id}")]
        public IHttpActionResult DeleteBooks(int id)
        {
            var books = new Book { BookId = id };
            bookStoreContext.Books.Attach(books);
            bookStoreContext.Books.Remove(books);
            bookStoreContext.SaveChanges();
            return Ok(id);
         }

            //// GET api/<controller>
            //[Route("api/Product/GetProducts")]
            //public IEnumerable<BooksJSON> GetBooks()
            //{
            //    IQueryable<BooksJSON> products = bookStoreContext.Books.Select(
            //            p => new BooksJSON
            //            {
            //                AuthorID = p.AuthorID,
            //                CateID = p.CateID,
            //                Title = p.Title,
            //                Summary = p.Summary,
            //                Quantity = p.Quantity,
            //                Price = p.Price,
            //                ImgUrl = p.ImgUrl,
            //                IsActive = p.IsActive,
            //                ISBN13 = p.ISBN13,
            //                ModifiedDate = p.ModifiedDate,
            //                PubID = p.PubID,
            //                CreatedDate = p.CreatedDate,
            //            });
            //    return products.ToList();
            //}
            //// POST api/<controller>
            //public Book Post([FromBody]Book book)
            //{
            //    if (book == null)
            //    {
            //        throw new ArgumentNullException("book");
            //    }

            //    Book newbook = new Book();

            //    try
            //    {
            //        newbook.AuthorID = book.AuthorID;
            //        newbook.CateID = book.CateID;
            //        newbook.Title = book.Title;
            //        newbook.Summary = book.Summary;
            //        newbook.Quantity = book.Quantity;
            //        newbook.Price = book.Price;
            //        newbook.ImgUrl = book.ImgUrl;
            //        newbook.IsActive = book.IsActive;
            //        newbook.ISBN13 = book.ISBN13;
            //        newbook.ModifiedDate = book.ModifiedDate;
            //        newbook.PubID = book.PubID;
            //        newbook.CreatedDate = book.CreatedDate;
            //        bookStoreContext.Books.Add(newbook);
            //        int rowsAffected = bookStoreContext.SaveChanges();

            //        return rowsAffected > 0 ? book : null;
            //    }
            //    catch (Exception e)
            //    {
            //        throw e;
            //    }
            //}
            //// PUT api/<controller>/5
            //public bool Put(int id, [FromBody]Book b)
            //{
            //    b.BookId = id;

            //    if (b == null)
            //    {
            //        throw new ArgumentNullException("book");
            //    }

            //    using (var ctx = new BookStoreEntities())
            //    {
            //        var books = bookStoreContext.Books.Single(a => a.BookId == b.BookId);

            //        if (books != null)
            //        {
            //            books.AuthorID = b.AuthorID;
            //            books.CateID = b.CateID;
            //            books.Title = b.Title;
            //            books.Summary = b.Summary;
            //            books.Quantity = b.Quantity;
            //            books.Price = b.Price;
            //            books.ImgUrl = b.ImgUrl;
            //            books.IsActive = b.IsActive;
            //            books.ISBN13 = b.ISBN13;
            //            books.ModifiedDate = b.ModifiedDate;
            //            books.PubID = b.PubID;
            //            books.CreatedDate = b.CreatedDate;
            //            bookStoreContext.Books.Add(books);

            //            int rowsAffected = bookStoreContext.SaveChanges();

            //            return rowsAffected > 0 ? true : false;
            //        }
            //        else
            //        {
            //            return false;
            //        }
            //    }
            //}
            //// DELETE api/<controller>/5
            //public bool Delete(int id)
            //{
            //    using (var ctx = new BookStoreEntities())
            //    {
            //        Book book = ctx.Books.Find(id);
            //        ctx.Books.Remove(book);

            //        int rowsAffected = ctx.SaveChanges();

            //        return rowsAffected > 0 ? true : false;
            //    }
            //}



        }
    }
